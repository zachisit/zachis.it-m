<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<!--we need this for plugins-->
<?php wp_head(); ?>

<!-- ga script added 01.25.2012 -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21726587-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- end ga script -->

</head>
<body>


<div id="container">
<div id="header">
<div id="header_cont">
		<h2><a href="http://zachis.it/m"><span class="logo_one">zachis.it</span></a></h2>
<div id="menu">
	<ul>



<li<?php  if (is_page('work')) { echo " id=\"current\""; }?>> <a href="http://zachis.it/m/work/" title="Wordpress and PHP and mobile portfolio">Work</a></li>
<li<?php  if (is_page('About') || is_page('about-zachary') || is_page('about-the-team') || is_page('about-company')) 
{ echo " id=\"current\""; }?>> <a href="http://www.zachis.it/m/ab/" title="about-2">About</a></li>
<li<?php  if (is_page('Contact')) { echo " id=\"current\""; }?>> <a href="http://www.zachis.it/m/contact/" title="contact">Contact</a></li>
<li<?php  if (is_page('Lifestream')) { echo " id=\"current\""; }?>> <a href="http://www.zachis.it/m/lifestream/" title="lifestream">Lifestream</a></li>
	</ul>
	</div>
</div><!-- header_cont end -->
</div><!--header.php end-->
