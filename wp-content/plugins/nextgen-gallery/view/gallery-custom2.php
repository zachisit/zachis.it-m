<?php 
/**
Template Page for the gallery overview

Follow variables are useable :

	$gallery     : Contain all about the gallery
	$images      : Contain all images, path, title
	$pagination  : Contain the pagination content

 You can check the content when you insert the tag <?php var_dump($variable) ?>
 If you would like to show the timestamp of the image ,you can use <?php echo $exif['created_timestamp'] ?>
**/
?>
<?php if (!defined ('ABSPATH')) die ('No direct access allowed'); ?><?php if (!empty ($gallery)) : ?>

<div class="ngg-galleryoverview" id="<?php echo $gallery->anchor ?>">

<?php if ($gallery->show_slideshow) { ?>
	<!-- Slideshow link -->
	<div class="slideshowlink">
		<a class="slideshowlink" href="<?php echo $gallery->slideshow_link ?>">
			<?php echo $gallery->slideshow_link_text ?>
		</a>
	</div>
<?php } ?>

<?php if ($gallery->show_piclens) { ?>
	<!-- Piclense link -->
	<div class="piclenselink">
		<a class="piclenselink" href="<?php echo $gallery->piclens_link ?>">
			<?php _e('[View with PicLens]','nggallery'); ?>
		</a>
	</div>
<?php } ?>
	
    <?php //echo '<pre>';print_r($images);echo '</pre>' ?>
    
	<!-- Thumbnails -->
	<?php $count = 1; foreach ( $images as $image ) : ?>
    <?php 
		if($count == 1) {
			$first_img_pid = $image->pid;
			$first_img_url = $image->imageURL;
			$first_img_desc = $image->description;
			$first_img_alt = $image->alttext;
			$first_img_thumb = $image->thumbnailURL;
			$first_img_size = $image->size;
		}
		if($count == 2) {
			$second_img_pid = $image->pid;
			$second_img_url = $image->imageURL;
			$second_img_desc = $image->description;
			$second_img_alt = $image->alttext;
			$second_img_thumb = $image->thumbnailURL;
			$second_img_size = $image->size;
			break;
		}
		
	?>
    <?php $count++;endforeach; ?>
    
    
    
    <?php $count = 0; foreach ( $images as $image ) : $count++; ?>
    
    <?php if($count == 1) : ?>
	<div id="ngg-image-<?php echo $image->pid ?>" class="ngg-gallery-thumbnail-box" <?php echo $image->style;?>>
		<div class="ngg-gallery-thumbnail" >
			<a href="<?php echo $second_img_url ?>" title="<?php echo $second_img_desc ?>" rel="prettyPhoto[pp_<?php echo $gallery->anchor ?>]">
				<?php if ( !$image->hidden ) { ?>
				<img title="<?php echo $second_img_alt ?>" alt="<?php echo $second_img_alt ?>" src="<?php echo $first_img_thumb ?>" <?php echo $first_img_size ?> />
				<?php } ?>
			</a>
		</div>
	</div>    
    <?php endif ?>
        
    <?php if($count >= 3) : ?>
    <div style="display:none">
    
    
                <div id="ngg-image-<?php echo $image->pid ?>" class="ngg-gallery-thumbnail-box" <?php echo $image->style; ?> >
                    <div class="ngg-gallery-thumbnail" >
                        <a href="<?php echo $image->imageURL ?>" title="<?php echo $image->description ?>" rel="prettyPhoto[pp_<?php echo $gallery->anchor ?>]">
                            <?php if ( !$image->hidden ) { ?>
                            <img title="<?php echo $image->alttext ?>" alt="<?php echo $image->alttext ?>" src="<?php echo $image->thumbnailURL ?>" <?php echo $image->size ?> />
                            <?php } ?>
                        </a>
                    </div>
                </div>
                
                
	</div>
    <?php endif ?>

 	<?php endforeach; ?>
    
     	
	<!-- Pagination -->
 	<?php echo $pagination ?>
    
 	
</div>

<?php endif; ?>