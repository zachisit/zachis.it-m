<?php
/*
Plugin Name: Simple Jquery Lightbox
Description: Embed simple jquery lightbox for nextgen custom template
Author: Ridwan Arifandi
Version: 1.0.0.0

Author URI: https://www.odesk.com/users/Expert-Wordpress-Cakephp-Developer_~~99068fd7c2c09623?sid=49002&tot=1&pos=0

Copyright 2007-2011 by Alex Rabe & NextGEN DEV-Team

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/ 

//add action hook

add_action('init','jQueryRegisterScript');
add_action('init','jQueryRegisterStyle');

function jQueryRegisterScript()
{
	wp_register_script('jquery.lightbox'	,plugins_url('jquery-lightbox/js/jquery.lightbox-0.5.js'),array('jquery'));
	
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery.lightbox');
}

function jQueryRegisterStyle()
{
	wp_register_style('jquery.lightbox'		,plugins_url('jquery-lightbox/css/jquery.lightbox-0.5.css'));
	
	wp_enqueue_style('jquery.lightbox');
}

?>
