<?php 
/**
Template Page for the gallery overview

Follow variables are useable :

	$gallery     : Contain all about the gallery
	$images      : Contain all images, path, title
	$pagination  : Contain the pagination content

 You can check the content when you insert the tag <?php var_dump($variable) ?>
 If you would like to show the timestamp of the image ,you can use <?php echo $exif['created_timestamp'] ?>
**/

if (!defined ('ABSPATH')) die ('No direct access allowed'); ?>
<?php if (!empty ($gallery)) : 
	
	foreach ( $images as $now => $image ) : 

	if($now == 0) :
	?>
    
	<div id="ngg-image-<?php echo $image->pid ?>" class="ngg-gallery-thumbnail-box" <?php echo $image->style ?> >
		<div class="ngg-gallery-thumbnail" >
			<a href="#ngg-modal-<?php echo $images[1]->pid ?>" title="<?php echo $image->description ?>" class="nyroModal" >
			<img title="<?php echo $image->alttext ?>" alt="<?php echo $image->alttext ?>" src="<?php echo $image->thumbnailURL ?>" <?php echo $image->size ?> />
			</a>
		</div>
	</div>
	<?php
	else :
	?>
	
	<div id="ngg-modal-<?php echo $image->pid ?>" class="ngg-modal <?php echo $image->style ?>" style="display:none;" >
    	<div >
		<div class="ngg-modal-holder">
        	<img src="<?php echo $image->imageURL; ?>" title="<?php echo $image->description; ?>" alt="" />
        </div>
        <div class="ngg-modal-content">
            <div class="ngg-modal-right">
            <?php 
			foreach( $images as $key => $dot ) : 
				if($key <> 0) :
					if($key == $now) :
					?><a href="#"><img src="<?php echo NGGALLERY_URLPATH; ?>/custom/img/dot-active.png" alt="" border="0" /></a><?php 
					else :
					?>
					<a href="#ngg-modal-<?php echo $dot->pid; ?>" class="nyroModal">
						<img src="<?php echo NGGALLERY_URLPATH; ?>/custom/img/dot-not-active.png" alt="" border="0" />
					</a>
					<?php 
					endif;
				endif;
			endforeach; 
			?>
            </div>
            <?php echo $image->description; ?>
            <div style="clear:both"></div>
        </div>
        </div>
	</div>
<?php 
	endif;
	endforeach; 
?>

<div style="clear:both"></div>

<?php endif; ?>